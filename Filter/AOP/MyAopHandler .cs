﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Runtime.Remoting.Messaging;
using System.Text;

namespace Filter.AOP
{
    public class MyAopHandler : IMessageSink
    {
        /// <summary>
        /// 下一个接收器
        /// </summary>
        public IMessageSink NextSink { get; private set; }
        public MyAopHandler(IMessageSink nextSink)
        {
            this.NextSink = nextSink;
        }

        /// <summary>
        /// 同步处理方法
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public IMessage SyncProcessMessage(IMessage msg)
        {
            //方法调用消息接口
            var call = msg as IMethodCallMessage;

            //只拦截指定方法,其它方法原样释放
            if (call == null || (Attribute.GetCustomAttribute(call.MethodBase, typeof(AOPMethodAttribute))) == null || call.MethodName != "add")
                try
                {
                    call.MethodBase.Invoke(call.MethodBase, call.Args);
                    NextSink.SyncProcessMessage(msg);
                }
                catch (Exception ex)
                {
                    string ll = ex.ToString();
                }

            //判断action是否有CheckLogin特性
            //bool isNeedLogin = filterContext.ActionDescriptor.IsDefined(typeof(CheckLogin), false);

            //判断第2个参数,如果是0,则强行返回100,不调用方法了
            if (((int)call.InArgs[1]) == 0) return new ReturnMessage(100, call.Args, call.ArgCount, call.LogicalCallContext, call);

            //判断第2个参数,如果是1,则参数强行改为50(失败了)
            //if (((int)call.InArgs[1]) == 1) call = new MyCall(call, call.Args[0], 50);//方法1  失败了
            //if (((int)call.InArgs[1]) == 1) call.MethodBase.Invoke(GetUnwrappedServer(), new object[] { call.Args[0], 50 });//方法2 (无法凑够参数)

            var retMsg = NextSink.SyncProcessMessage(call); call.MethodBase.Invoke(call, call.Args);

            //判断返回值,如果是5,则强行改为500
            if (((int)(retMsg as IMethodReturnMessage).ReturnValue) == 5) return new ReturnMessage(500, call.Args, call.ArgCount, call.LogicalCallContext, call);

            return retMsg;           
        }

        /// <summary>
        /// 异步处理方法(暂不处理)
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="replySink"></param>
        /// <returns></returns>
        public IMessageCtrl AsyncProcessMessage(IMessage msg, IMessageSink replySink) { return null; }
    }

    /// <summary>
    /// 贴在方法上的标签
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    public sealed class AOPMethodAttribute : Attribute { }

    /// <summary>
    /// 贴在方法上的标签
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    public sealed class AO1P1MethodAttribute : Attribute 
    {
        public int j = 0;
    }

    /// <summary>
    /// 贴在类上的标签
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
    public sealed class AOPAttribute : ContextAttribute, IContributeObjectSink
    {
        public AOPAttribute() : base("AOP") 
        {

        }

        /// <summary>
        /// 实现消息接收器接口
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="next"></param>
        /// <returns></returns>
        public IMessageSink GetObjectSink(MarshalByRefObject obj, IMessageSink next) 
        { 
            return new MyAopHandler(next);
        }
    }
}
